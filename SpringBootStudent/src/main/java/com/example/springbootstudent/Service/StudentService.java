package com.example.springbootstudent.Service;

import com.example.springbootstudent.Entities.Student;
import com.example.springbootstudent.Repositories.StudentRepo;
import com.example.springbootstudent.Request.StudentRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Optional;
import java.util.Random;

@Service
public class StudentService
{
    @Autowired
    private final StudentRepo studentRepo;


    public StudentService(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }

    public String nutzerRegistrieren(Student student)
    {
        boolean studentExistiert = studentRepo.findByEmail(student.getEmail()).isPresent();

        if (studentExistiert)
        {
            throw new IllegalStateException("Die Email ist bereits vergeben");
        }

        Random matrikelnummerRandom = new Random();
        int num = matrikelnummerRandom.nextInt(999999) + 1000000;
        student.setMatrikelnummer(num);

        boolean matrikelExistiert = studentRepo.findByMatrikelnummer(student.getMatrikelnummer()).isPresent();

        while(matrikelExistiert){
            Random matrikelnummerRandom2 = new Random();
            int num2 = matrikelnummerRandom2.nextInt(999999) + 1000000;
            student.setMatrikelnummer(num2);
            matrikelExistiert = studentRepo.findByMatrikelnummer(student.getMatrikelnummer()).isPresent();

        }

        studentRepo.save(student);
        return "Student wurde erfolgreich gespeichert";

    }

    public String registrieren(StudentRequest studentRequest)
    {
        return nutzerRegistrieren(new Student(studentRequest.getVorname(), studentRequest.getNachname(), studentRequest.getEmail(), studentRequest.getPasswort(), studentRequest.getProfilbild()));
    }

    public Optional <Student> login(String email, String passwort)
    {
       Optional<Student> student = studentRepo.findByEmailAndPasswort(email, passwort);
       return student;
    }
}
