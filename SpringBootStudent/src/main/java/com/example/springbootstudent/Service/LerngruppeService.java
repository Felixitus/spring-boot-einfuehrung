package com.example.springbootstudent.Service;

import com.example.springbootstudent.Entities.Student;
import com.example.springbootstudent.Repositories.LerngruppeRepo;
import com.example.springbootstudent.Repositories.StudentRepo;
import com.example.springbootstudent.Request.LerngruppeRequest;
import com.example.springbootstudent.Entities.Lerngruppe;
import org.springframework.stereotype.Service;

@Service
public class LerngruppeService
{
    private final LerngruppeRepo lerngrppeRepo;
    private final StudentRepo studentRepo;

    public LerngruppeService(LerngruppeRepo lerngrppeRepo, StudentRepo studentRepo)
    {
        this.lerngrppeRepo = lerngrppeRepo;
        this.studentRepo = studentRepo;
    }

    public String erzeugeLerngruppe(LerngruppeRequest lerngruppeRequest, int id)
    {
        Student student = studentRepo.findById(id);

        Lerngruppe lerngruppe = new Lerngruppe(lerngruppeRequest.getName(), lerngruppeRequest.getBeschreibung());
        lerngruppe.erzeuger(student);
        lerngrppeRepo.save(lerngruppe);

        return "Lerngruppe wurde erfolgreich angelegt";
    }

    public String studentHinzufugen(int gruppenId, int studentenId)
    {
        Lerngruppe lerngruppe = lerngrppeRepo.findById(gruppenId);
        Student student = studentRepo.findById(studentenId);
        lerngruppe.gruppeStudenten(student);
        lerngrppeRepo.save(lerngruppe);

        return "Der Student wurde erfolgreich hinzugefügt";
    }
}
