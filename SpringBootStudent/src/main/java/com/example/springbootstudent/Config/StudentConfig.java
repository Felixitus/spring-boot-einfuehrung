package com.example.springbootstudent.Config;

import com.example.springbootstudent.Entities.Student;
import com.example.springbootstudent.Repositories.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class StudentConfig
{

    @Autowired
    private final StudentRepo studentRepo;

    public StudentConfig(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }

    @Bean
    CommandLineRunner commandLineRunner()
    {
        return args ->
        {
            Student student1 = new Student
                    (
                      "Felix",
                      "Spiss",
                      "spiss.felix@gmail.com",
                       12345678,
                       "pass",
                            null
                    );

            Student student2 = new Student
                    (
                            "Philipp",
                            "Hackl",
                            "phhackl@tsn.at",
                            12345677,
                            "pass",
                            null
                    );
            if (studentRepo.findAll().size() == 0)
            studentRepo.saveAll(
                    List.of(student1, student2)
            );
        };
    }
}
